public class Enemy {
    private String name;
    private int health;
    private int attackPoints;


    public Enemy(String name, int health, int attackPoints) {
        this.name = name;
        this.health = health;
        this.attackPoints = attackPoints;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public void removeHealthPoints(int damage){
        this.health -= damage;
        if(health < 0){
            health = 0;
        }
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }

    public int getAttackPoints() {
        return attackPoints;
    }


}
