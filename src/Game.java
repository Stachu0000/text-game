import java.util.ArrayList;
import java.util.Scanner;

public class Game {
    Scanner scanner = new Scanner(System.in);
    Player player = new Player();
    Story story = new Story();
    String currentLocation = "Ulica";
    Inventory inventory = new Inventory();
    Location location = new Location();
    ArrayList<Location> buildings = location.getLocations();


    public void PlayGame() {

        player.setName();
        System.out.println(story.getIntro());
        inventory.items.add(new Item("Scyzoryk"));
        playerStatus();
        takeYourPath();

    }

    public void takeYourPath() {
        boolean choice = true;
        while (choice) {
            System.out.println("Oto budynki. Gdzie chcesz pójść? Wpisz nazwę budynku " +
                    "lub słowo \"status\" by sprawdzić stan gracza.");
            buildings.forEach(b -> System.out.println(b.getName()));

            String input = scanner.nextLine();
            switch (input) {
                case "Dom":
                    System.out.println("Drzwi były otwarte, jesteś w domu");
                    searchingItems(buildings.get(0));
                    if (!hasPicklock()) {
                        System.out.println("\n@@\n@@ Cóż za cudowne zrządzenie losu, znajdujesz wytrych do drzwi :) @@ \n@@");
                        inventory.items.add(new Item("Wytrych"));
                    }
                    goOut();
                    //choice = false;
                    break;
                case "Apteka":
                    Location pharmacy = buildings.get(1);
                    boolean isPharmacyOpen = pharmacy.isOpen();

                    if (!isPharmacyOpen && !hasPicklock()) {
                        System.out.println("Ups drzwi są zamknięte. Poszukaj czegoś do otwarcia.");
                        goOut();
                    } else if (!isPharmacyOpen && hasPicklock()) {
                        pharmacy.setOpen(true);
                        System.out.println("Otworzyłeś drzwi wytyrchem, jesteś w aptece.");
                        fightingOrRunning(pharmacy);

                    } else if (pharmacy.getEnemy() != null) {
                        fightingOrRunning(pharmacy);
                    } else {
                        searchingItems(pharmacy);
                        goOut();
                    }
                    currentLocation = "Apteka";
                    break;
                case "Piekarnia":
                    Location bakery = buildings.get(2);
                    boolean isBakeryOpen = bakery.isOpen();

                    if (!isBakeryOpen && !hasPicklock()) {
                        System.out.println("Ups drzwi są zamknięte. Poszukaj czegoś do otwarcia.");
                        goOut();
                    } else if (!isBakeryOpen && hasPicklock()) {
                        bakery.setOpen(true);
                        System.out.println("Otworzyłeś drzwi wytyrchem, jesteś w piekarni.");
                        fightingOrRunning(bakery);

                    } else if (bakery.getEnemy() != null) {
                        fightingOrRunning(bakery);
                    } else {
                        searchingItems(bakery);
                        goOut();
                    }

                    currentLocation = "Piekarnia";
                    break;
                case "Piwnica":
                    Location basement = buildings.get(3);
                    boolean isBasementOpen = basement.isOpen();

                    if (!isBasementOpen && !hasPicklock()) {
                        System.out.println("Ups drzwi są zamknięte. Poszukaj czegoś do otwarcia.");
                        goOut();
                    } else if (!isBasementOpen && hasPicklock()) {
                        basement.setOpen(true);
                        System.out.println("Otworzyłeś drzwi wytyrchem, jesteś w piwnicy.");
                        fightingOrRunning(basement);

                    } else if (basement.getEnemy() != null) {
                        fightingOrRunning(basement);
                    } else {
                        searchingItems(basement);
                        goOut();
                    }
                    currentLocation = "Piwnica";
                    break;
                case "status":
                    playerStatus();
                    break;
                default:
                    System.out.println("Podaj właściwy budynek");
            }

        }
    }

    public boolean choiceFightOrRun(Location building) {
        if (inventory.getWeapon() == null) {
            System.out.println("Nie masz broni, wię uciekasz.");
            return false;
        }

        System.out.println("Motyla noga, " + building.getEnemy().getName() + " jest w środku. Walczysz czy uciekasz?\n " +
                "Wpisz \"walka\" lub \"ucieczka\"");

        while (true) {
            String input = scanner.nextLine();

            if (input.equals("walka")) {
                return true;
            } else if (input.equals("ucieczka")) {
                return false;
            } else {
                System.out.println("Wpisz \"walka\" lub \"ucieczka\"");
            }
        }

    }

    public boolean Fight(Location building) {
        while (true) {
            System.out.println("Wpisz \"Jeb\" żeby uderzyć lub uciekaj z budynku. Wpisz \"spadam\"");
            String input = scanner.nextLine();
            Enemy zombie = building.getEnemy();
            if (input.equals("Jeb")) {
                int playerAttackPoints = inventory.getWeapon().getDamage();
                int enemyAttackPoints = zombie.getAttackPoints();

                zombie.removeHealthPoints(playerAttackPoints);
                player.removeHealthPoints(enemyAttackPoints);

                isPlayerAlive();

                System.out.println("Zostało Ci: " + player.getHealth() + " punktów życia a zombiakowi: "
                        + zombie.getHealth() + " punktów.");
            } else if (input.equals("spadam")) {
                return false;
            } else {
                player.removeHealthPoints(zombie.getAttackPoints());
                isPlayerAlive();
                System.out.println("Żle wpisałeś czyli dostałeś bęcki, zostało Ci " + player.getHealth() + "punktów życia.");
            }

            if (zombie.getHealth() <= 0) {
                System.out.println("Brawo, ubiłeś dziada. Możesz teraz zobaczyć czy w lokalu są jakieś fanty");
                building.setEnemy(null);
                return true;
            }
        }
    }

    public void searchingItems(Location location) {

        Weapon weapon = location.getWeapon();
        MedItem medItem = location.getMedItem();


        if (weapon == null && medItem == null) {
            System.out.println("!!!Zabrałeś już wszystkie dostępne przedmioty z tego budynku!!!");
        } else {
            System.out.println("Po gruntownym przeszukaniu pomieszczenia znajdujesz:");
        }

        if (weapon != null) {
            System.out.println("-- Broń: " + weapon.getName() + " ---Damage: " + weapon.getDamage());
            System.out.println("Chcesz zamienić aktualną broń na " + weapon.getName() + "? To wpisz \"tak\"");
            String input = scanner.nextLine();
            if (input.equals("tak")) {
                inventory.setWeapon(weapon);
                location.setWeapon(null);
                weapon = null;
            }
        }
        if (location.getName().equals("Dom")) {
            System.out.println("-- Cukierki anyżowe: " + medItem.getName()
                    + " ---Health Points: " + medItem.getHealthPoints() + " Może i są niedobre ale za to " +
                    "możesz je jeść bez limitu.");
            System.out.println("Chcesz zjeść: " + medItem.getName() + " To wpisz \"tak\"");
            String input = scanner.nextLine();
            if (input.equals("tak")) {
                player.setHealth(medItem.getHealthPoints());

            }
        }
        if (medItem != null && !location.getName().equals("Dom")) {
            System.out.println("-- przedmiot zwiększający energię: " + medItem.getName()
                    + " ---Health Points: " + medItem.getHealthPoints());
            System.out.println("Chcesz zużyć: " + medItem.getName() + " To wpisz \"tak\"");
            String input = scanner.nextLine();
            if (input.equals("tak")) {
                player.setHealth(medItem.getHealthPoints());
                location.setMedItem(null);
                medItem = null;

            }
        }

        if (location.getName().equals("Piwnica") && weapon == null && medItem == null) {
            endingGame();
        }


    }

    public void goOut() {
        System.out.println("\n\nWróciłeś na ulicę.\n\n");
        currentLocation = "Ulica";
    }

    public void isPlayerAlive() {
        if (player.getHealth() <= 0) {
            System.out.println("\n\nNiestety poległeś, ostatnia nadzieja ludzkości zmarnowana, ehhhh...\nKoniec gry!!!");
            System.exit(0);
        }
    }

    public boolean hasPicklock() {
        return inventory.items.stream().anyMatch(i -> i.getName().equals("Wytrych"));
    }

    public void playerStatus() {
        System.out.println("-------------------------");
        System.out.println("Aktualny status gracza:");
        System.out.println("*** Zdrowie: " + player.getHealth());
        inventory.items.forEach(i -> System.out.println("*** " + i.getName()));
        if (inventory.getWeapon() != null) {
            System.out.println("*** Broń: " + inventory.getWeapon().getName() + " ---Damage: "
                    + inventory.getWeapon().getDamage());
        }
        System.out.println("-------------------------");

    }

    public void fightingOrRunning(Location building) {
        boolean choiceResult = choiceFightOrRun(building);
        if (choiceResult) {
            boolean fightResult = Fight(building);
            if (fightResult) {
                searchingItems(building);
                goOut();
            } else {
                goOut();
            }
        } else {
            goOut();
        }
    }

    public void endingGame() {
        System.out.println(story.getOutro());
        System.exit(0);
    }
}
