public class Story {
    private String intro = "Rok 2024. Świat zmienił się w miejsce, zrujnowane przez apokalipsę," +
            "której źródłem było AI o nazwie Chad DżPiTi. \n" +
            "Tajemniczy program stworzył substancję opartą o lewoskrętną witaminę C, \n" +
            "którą wprowadziła do popularnego piwa o nazwie \"Karaś\". \n" +
            "Aby zwiększyć zasięg rażenia Chad DżPiTi wprowadził „Karasia” do sieci sklepów spożywczych o nazwie \n" +
            "\"Twoja Stonka\", która przez lata budowała swoją dominację na rynku. \n" +
            "Aby szybko dokonać ataku Skynet eee tzn. Chad DżPiTi zrobił giga promocję „20 piw w cenie 1”. \n" +
            "Nieświadomi konsekwencji konsumenci pili, a wszyscy, którzy się zetknęli z tym napojem, \n" +
            "zostali przemienieni w niebezpieczne zombie.\n" +
            "Teraz, jako jedyny ocalały, stajesz przed zadaniem przetrwania i odkrywania prawdy o tym, \n" +
            "co spowodowało zagładę ludzkości.\n" +
            "Czas nie jest po twojej stronie, a hordy zombie czyhają wszędzie." +
            "Twoje decyzje kształtują losy przyszłości, \n " +
            "a odpowiedzi na pytania leżą ukryte w cieniach tego apokaliptycznego miasteczka. \n" +
            "Jedyną broń jaką posiadasz do stary scyzoryk z wygrawerowanym napisem" +
            " „Nie ma już pracy dla juniorów”.\n" +
            "\n" +
            "Jak się gra: \n" +
            "Masz do wyboru 4 lokacje, w każdej z nich możesz napotkać na zombiaka i/lub zasoby.\n" +
            "Walka to na twardo wet za wet, jakbyś miał przegrać to możesz taktycznie się wycofać i " +
            "farmić zdrowie z niewyczerpalnych żródeł." +
            "\nPowodzenia, buu haha 'złowiesczy śmiech'\n";

    private String outro = "-Hurrrra, pokonałeś najgoźniejszego zombiaka 'Yarosława' i zebrałeś najlepze fanty z jego miejscówy\n" +
            "--Dalsza część tej niezwykłeś przygody dostępna jako płatne DLC. \n" +
            "---Ale, zaraz, co to? Znajdujesz w kieszeni zombiaka kopetę, którą otwierasz scyzorykiem. A w środku kartka: \n" +
            "---'''Po co się uczyć na juniora, lepiej odrazu na mida!!!'''\n" +
            "----Kurtyna...";

    public String getOutro() {
        return outro;
    }

    public String getIntro() {
        return intro;
    }


}
