public class Item implements Names{
    private String name;

    public Item(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }
}
