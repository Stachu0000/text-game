import java.util.Scanner;

public class Player {

    private String name;
    private int health = 90;
    private Inventory inventory;

    public void setName() {
        System.out.println("Witaj bohaterze, podaj swoje imię!");
        this.name = scanner.nextLine();
    }

    public String getName() {
        return name;
    }
    Scanner scanner = new Scanner(System.in);

    public void removeHealthPoints(int damagePoints){
        health -= damagePoints;
    }

    public void setHealth(int healthPoints) {
        health += healthPoints;
        if(health >= 100){
            health = 100;
        }
    }
    public int getHealth(){
        return health;
    }
}
