public class MedItem implements Names{
    private String name;
    private int healthPoints;

    public MedItem(String name, int healthPoints) {
        this.name = name;
        this.healthPoints = healthPoints;
    }

    @Override
    public String getName() {
        return name;
    }

    public int getHealthPoints(){return healthPoints;}
}
