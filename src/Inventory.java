import java.util.ArrayList;

public class Inventory {
    private Weapon weapon;
    private MedItem medItem;
    public ArrayList<Item> items = new ArrayList<>();

    public Weapon getWeapon() {
        return weapon;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    public MedItem getMedItem() {
        return medItem;
    }



    public void setMedItem(MedItem medItem) {
        this.medItem = medItem;
    }
}
