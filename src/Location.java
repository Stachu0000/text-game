import java.util.ArrayList;

public class Location {
    private String name;
    private Enemy enemy;
    private Weapon weapon;
    private MedItem medItem;
    private boolean isOpen;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    public MedItem getMedItem() {
        return medItem;
    }

    public void setMedItem(MedItem medItem) {
        this.medItem = medItem;
    }

    public Enemy getEnemy() {
        return enemy;
    }

    public void setEnemy(Enemy enemy) {
        this.enemy = enemy;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        this.isOpen = open;
    }


    public ArrayList<Location> getLocations() {
        ArrayList<Location> locations = new ArrayList<>();

        Location building1 = new Location();
        building1.setName("Dom");
        building1.isOpen = true;
        building1.setMedItem(new MedItem("Anyżki", 10));
        building1.setWeapon(new Weapon("Kij od szczotki", 20));
        locations.add(building1);

        Location building2 = new Location();
        building2.setName("Apteka");
        building2.isOpen = false;
        building2.setMedItem(new MedItem("SmallMed", 60));
        building2.setWeapon(new Weapon("Kij do basebola", 30));
        Enemy zombie1 = new Enemy("Zombie: 'Ardiano'", 80, 20);
        building2.setEnemy(zombie1);
        locations.add(building2);

        Location building3 = new Location();
        building3.setName("Piekarnia");
        building3.setMedItem(new MedItem("MediumMed", 80));
        building3.setWeapon(new Weapon("Kij do basebola", 40));
        Enemy zombie2 = new Enemy("Zombie: 'Mateo'", 100, 30);
        building3.setEnemy(zombie2);
        locations.add(building3);

        Location building4 = new Location();
        building4.setName("Piwnica");
        building4.setMedItem(new MedItem("BigMed", 100));
        building4.setWeapon(new Weapon("Maczeta", 30));
        Enemy zombie3 = new Enemy("Zombie: 'Yaro'", 120, 30);
        building4.setEnemy(zombie3);
        locations.add(building4);

        return locations;

    }
}
